import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
require('firebase/database');

const app = firebase.initializeApp({
    apiKey: "AIzaSyCO6rxXATpKWd0_s8lN6xmYWAjHl5hSf2E",
    authDomain: "progressive-to-do-list.firebaseapp.com",
    databaseURL: "https://progressive-to-do-list-default-rtdb.firebaseio.com",
    projectId: "progressive-to-do-list",
    storageBucket: "progressive-to-do-list.appspot.com",
    messagingSenderId: "488256942335",
    appId: "1:488256942335:web:dee8285548161ad30b5cca"
})




export default app