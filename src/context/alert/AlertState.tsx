import React, { useReducer } from 'react'
import { AlertContext } from './alertContext'
import { alertInitialState, alertReducer, PayloadType, showAlertAC, hideAlertAC } from './alertReducer'


const AlertState: React.FC<any> = ({ children }) => {
    const [state, dispatch] = useReducer(alertReducer, alertInitialState)

    const show = (payload: PayloadType) => {
        dispatch(showAlertAC(payload))
    }

    const hide = () => {
        dispatch(hideAlertAC())
    }
    return (
        <AlertContext.Provider value={{ show, hide, alert: state }}>
            {children}
        </AlertContext.Provider>
    )
}

export default AlertState