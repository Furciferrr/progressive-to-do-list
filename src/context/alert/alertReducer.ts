const SHOW_ALERT = 'SHOW_ALERT'
const HIDE_ALERT = 'HIDE_ALERT'

export type PayloadType = {
    text: string
    type: string
}
type ActionType = {
    type: 'SHOW_ALERT' | 'HIDE_ALERT'
    payload: PayloadType
}

export let alertInitialState = {
    visible: false as boolean,
    payload: {
        text: '',
        type: ''
    } as PayloadType
}
export type initialStateType = typeof alertInitialState
export const alertReducer = (state = alertInitialState, action: ActionType): initialStateType => {
    switch (action.type) {
        case 'HIDE_ALERT': {
            return {
                ...state,
                visible: false,
                payload: action.payload
            }
        }
        case 'SHOW_ALERT': {
            return {
                ...state,
                visible: true,
                payload: action.payload
            }
        }

        default:
            return state;
    }
}

export const showAlertAC = (payload: PayloadType): ActionType => {
    return ({
        type: SHOW_ALERT,
        payload
    })
}

export const hideAlertAC = (): ActionType => {
    return ({
        type: HIDE_ALERT,
        payload: {
            text: '',
            type: ''
        }
    })
}