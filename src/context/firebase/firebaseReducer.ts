const SHOW_LOADER = 'SHOW_LOADER'
const ADD_NOTE = 'ADD_NOTE'
const FETCH_NOTE = 'FETCH_NOTE'
const REMOVE_NOTE = 'REMOVE_NOTE'
const ADD_USER_DATA = 'ADD_USER_DATA'
const CHENGE_IS_AUTH = 'CHENGE_IS_AUTH'

export type NoteType = {
    id: string
    title: string
    date: string
}

type PropertyTypes<T> = T extends {[key: string]: infer U} ? U : never
type InferActionsTypes<T extends {[key: string]: (...args: any[])=> any}> = ReturnType<PropertyTypes<T>>
type ActionTypes = InferActionsTypes<typeof actionsFirebase>


export let firebaseInitialState = {
    notes: [] as Array<NoteType>,
    loading: false as boolean,
    email: '' as string,
    userRegistrId: '' as string,
    userId: '' as string,
    isAuth: false as boolean
}

export type initialStateType = typeof firebaseInitialState
export const firebaseReducer = (state = firebaseInitialState, action: ActionTypes): initialStateType => {
    switch (action.type) {
        case 'SHOW_LOADER': {
            return {
                ...state, loading: true
            }
        }
        case 'ADD_NOTE': {
            return {
                ...state,
                notes: [...state.notes, action.payload]
            }
        }
        case 'FETCH_NOTE': {
            return {
                ...state,
                notes: action.payload,
                loading: false
            }
        }
        case 'REMOVE_NOTE': {
            return {
                ...state,
                notes: state.notes.filter(note => {
                    return note.id !== action.id
                })
            }
        }
        case 'ADD_USER_DATA': {
            return {
                ...state, email: action.email,
                userRegistrId: action.userRegistrId,
                userId: action.userId
            }
        }
        case 'CHENGE_IS_AUTH': {
            return {
                ...state, 
                isAuth: action.value
            }
        }

        default:
            return state;
    }
}

export const actionsFirebase = {
    showLoaderAC: () => {
        return ({
            type: SHOW_LOADER
        } as const)

    },
    fetchNoteAC: (payload: Array<NoteType>) => {
        return ({
            type: FETCH_NOTE,
            payload
        } as const)
    },
    addNoteAC: (payload: NoteType) => {
        return ({
            type: ADD_NOTE,
            payload: payload
        } as const)
    },
    removeNoteAC: (id: string) => {
        return ({
            type: REMOVE_NOTE,
            id
        } as const)
    },
    addUserData: (email: string, userRegistrId: string, userId: string) => {
        return ({
            type: ADD_USER_DATA,
            email, userRegistrId, userId
        } as const)
    },
    chengeIsAuth: (value: boolean) => {
       return ({
           type: CHENGE_IS_AUTH,
           value
        } as const ) 
    }
}

