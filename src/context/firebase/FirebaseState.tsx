import React, { useReducer } from 'react'
import axios from 'axios'
import { FirebaseContext } from './firebaseContext'
import { firebaseInitialState, firebaseReducer, actionsFirebase } from './firebaseReducer'

const url = 'https://progressive-to-do-list-default-rtdb.firebaseio.com'

const FirebaseState: React.FC<any> = ({ children }) => {
    const [state, dispatch] = useReducer(firebaseReducer, firebaseInitialState)


    const showLoader = () => {
        dispatch(actionsFirebase.showLoaderAC())
    }

    const getCurrentUserData = async (uid: string | null | undefined) => {
        try {
            const res = await axios.get(`${url}/users.json`)
            Object.keys(res.data).forEach(key => {
                if (res.data[key].uid === uid) {

                    const payload = Object.keys(res.data[key].notes).map(k => {
                        return {
                            ...res.data[key].notes[k],
                            id: k
                        }
                    })

                    dispatch(actionsFirebase.fetchNoteAC(payload))
                    dispatch(actionsFirebase.addUserData(res.data[key].email, res.data[key].email, key))
                    dispatch(actionsFirebase.chengeIsAuth(true))
                }
            })
        } catch (e) {
            throw new Error(e.message)
        }
    }

    const logInLogOut = (value: boolean) => {
        dispatch(actionsFirebase.chengeIsAuth(value))
    }

    const fetchNotes = async (userId: string) => {
        if(userId) {
        try {
            const res = await axios.get(`${url}/users/${userId}/notes.json`)
            const payload = Object.keys(res.data).map(key => {
                return {
                    ...res.data[key],
                    id: key
                }
            })
            dispatch(actionsFirebase.fetchNoteAC(payload))
        } catch (e) {
            throw new Error(e.message)
        }
    }
    }


    const addNote = async (title: string, userId: string) => {
        const note = {
            title, date: new Date().toJSON()
        }
        try {
            const res = await axios.post(`${url}/users/${userId}/notes.json`, note)
            const payload = {
                ...note,
                id: res.data.name
            }
            dispatch(actionsFirebase.addNoteAC(payload))
        } catch (e) {
            throw new Error(e.message)
        }

    }

    const removeNote = async (id: string, userId: string) => {

        await axios.delete(`${url}/users/${userId}/notes/${id}.json`)
        dispatch(actionsFirebase.removeNoteAC(id))
    }
    return (
        <FirebaseContext.Provider value={{
            showLoader, fetchNotes, addNote, getCurrentUserData,
            removeNote, notes: state.notes, loading: state.loading, userId: state.userId, isAuth: state.isAuth, 
            email: state.email, logInLogOut
        }}>
            {children}
        </FirebaseContext.Provider>
    )
}

export default FirebaseState