import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router';
import Form from '../components/Form';
import { Loader } from '../components/Loader';
import Note from '../components/Notes';
import { FirebaseContext } from '../context/firebase/firebaseContext';


type HomePropsType = {

}

const Home: React.FC<HomePropsType> = (props) => {

  const { loading, notes, fetchNotes, removeNote, userId, isAuth } = useContext(FirebaseContext)
  const history = useHistory()

  useEffect(() => {
    fetchNotes(userId)
    // eslint-disable-next-line
  }, [])
  return (
    <>
      {isAuth ?
        <div>
          <Form />
          <hr />
          {loading ? <Loader /> : <Note notes={notes} removeNote={removeNote} userId={userId}/>}
        </div>
        : history.push('/login')
      }
    </>
  );
}

export default Home;
