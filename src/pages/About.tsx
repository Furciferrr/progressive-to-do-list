import React from 'react';
import { Container, Jumbotron } from 'react-bootstrap';


type AboutPropsType = {

}
const About: React.FC<AboutPropsType> = (props) => {
  return (
    <Jumbotron fluid>
    <Container>
      <h1>React App</h1>
      <p>
        React app with hooks and firebase
      </p>
    </Container>
  </Jumbotron>
  );
}

export default About;
