import React, { useContext } from 'react';
import { Alert } from 'react-bootstrap';
import { AlertContext } from '../context/alert/alertContext';
import { CSSTransition } from 'react-transition-group'


const AlertComponent: React.FC = () => {

  const { alert, hide } = useContext(AlertContext)

  return (
    <CSSTransition
      in={alert.visible}
      timeout={{
        enter: 1000,
        exit: 300
      }}
      classNames={'alert'}
      onExited={() => { hide() }}
      mountOnEnter
      unmountOnExit
    >
      <Alert variant={alert.payload.type} onClose={() => hide()} dismissible>
        <Alert.Heading>{alert.payload.text}</Alert.Heading>
        <p>
        </p>
      </Alert>
    </CSSTransition>

  );
}

export default AlertComponent;
