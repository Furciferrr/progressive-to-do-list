import React, { useContext, useState } from 'react';
import { AlertContext } from '../context/alert/alertContext';
import { FirebaseContext } from '../context/firebase/firebaseContext';


type FormPropsType = {

}
const Form: React.FC<FormPropsType> = (props) => {
  const [value, setValue] = useState<string>('')
  const alert = useContext(AlertContext)
  const firebase = useContext(FirebaseContext)

  const onChangeValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value)
  }

  const submitHandler = (e: React.FormEvent<EventTarget>) => {
    e.preventDefault()
    if(value.trim()) {
      firebase.addNote(value.trim(), firebase.userId).then(()=> {
        alert.show({text: 'note was created', type: 'success'})
        setTimeout(()=>{alert.hide()}, 2000)
      }).catch(()=>{
        alert.show({text: 'error', type: 'warning'})
      })
      setValue('')
    } else {
      alert.show({text: 'enter text', type: 'warning'})
    }
    
  }
  return (
    <form onSubmit={(e) => { submitHandler(e) }}>
      <div className="form-group">
        <input type="text" className="form-control" placeholder='enter note title'
          value={value} onChange={(e) => { onChangeValue(e) }} />
      </div>
    </form>
  );
}

export default Form;
