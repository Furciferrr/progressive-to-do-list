import React, { useContext, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { FirebaseContext } from '../../context/firebase/firebaseContext';
import app from './../../config/firebase'
import classes from './login.module.css';

type RegistrationPropsType = {
  /* getCurrentUserData: (uid: string | null | undefined) => void */
}

export const Registration: React.FC<RegistrationPropsType> = (props) => {
  const [emailValue, setEmailValue] = useState<string>('')
  const [passwordValue, setPasswordValue] = useState<string>('')
  const [errorMessage, setErrorMessage] = useState<string>('')

  const { getCurrentUserData } = useContext(FirebaseContext)
  const history = useHistory()

  const onChangeEmail = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmailValue(e.currentTarget.value)
  }
  const onChangePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPasswordValue(e.currentTarget.value)
  }



  const createNewUserOnDatabase = (uid: string | null | undefined, email: string | null | undefined) => {


    const newUser = {
      uid: uid,
      email: email,
      notes: {
        "-MWoTZALiApz-c_fqSEY": {
          "date": "2021-03-27T16:17:52.722Z",
          "title": "sdsds"
        }
      }
    }

    app.database().ref('users').push(newUser).then(resp => getCurrentUserData(newUser.uid))
      .catch(error => {
        setErrorMessage(error)
      })
  }

  const onRegistration = () => {

    app.auth().createUserWithEmailAndPassword(emailValue, passwordValue)
      .then(resp => createNewUserOnDatabase(resp.user?.uid, resp.user?.email)).then(res => history.push('/'))
      .catch((err) => setErrorMessage(err.message))

  }

  return (
    <div className={classes.formWrapper}>
      <div>SIGN UP</div>
      <input type='email' placeholder='email' value={emailValue} onChange={(e) => { onChangeEmail(e) }} />
      <input type='password' placeholder='password' value={passwordValue} onChange={(e) => { onChangePassword(e) }} />
      <button onClick={() => { onRegistration() }} className={classes.registerbtn}>Create Accaunt</button>
      <div className={classes.signin}>
        <p>Already have an account? <Link to={'/login'}>Sign in</Link></p>
      </div>
      <span className={classes.error}>{errorMessage}</span>
    </div>
  );
}

