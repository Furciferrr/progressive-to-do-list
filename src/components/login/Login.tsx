import React, { useContext, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import classes from './login.module.css';
import app from './../../config/firebase'
import { FirebaseContext } from '../../context/firebase/firebaseContext';

type LoginPropsType = {
 
}


export const Login: React.FC<LoginPropsType> = (props) => {
  const [emailValue, setEmailValue] = useState<string>('')
  const [passwordValue, setPasswordValue] = useState<string>('')
  const [errorMessage, setErrorMessage] = useState<string>('')
  const { getCurrentUserData } = useContext(FirebaseContext)
  const history = useHistory()

  const onChangeEmail = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmailValue(e.currentTarget.value)
  }
  const onChangePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPasswordValue(e.currentTarget.value)
  }

  

  const onLogin = () => {
    

    app.auth().signInWithEmailAndPassword(emailValue, passwordValue).then(resp => getCurrentUserData(resp.user?.uid)).then(res => history.push('/'))
    .catch((err)=>setErrorMessage(err.message))
    
  }

  return (
    <div className={classes.formWrapper}>
      <div>SIGN IN</div>
      <input type='email' placeholder='Enter Email' value={emailValue} onChange={(e) => { onChangeEmail(e) }} />
      <input type='password' placeholder='Enter Password' value={passwordValue} onChange={(e) => { onChangePassword(e) }} />
      <button onClick={() => { onLogin() }} className={classes.registerbtn}>Login</button>
      <div className={classes.signin}>
        <p>Create accaunt <Link to={'/registration'}>Registration</Link></p>
      </div>
      <span className={classes.error}>{errorMessage}</span>
    </div>
  );
}

