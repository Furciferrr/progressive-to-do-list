import React, { useContext } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { Navbar, Nav, Button } from 'react-bootstrap'
import { FirebaseContext } from '../context/firebase/firebaseContext';


type NavbarPropsType = {

}
const NavbarMenu: React.FC<NavbarPropsType> = (props) => {
  const { logInLogOut, isAuth, email } = useContext(FirebaseContext)
  const history = useHistory()
  return (
    <Navbar collapseOnSelect expand="sm" bg="primary" variant="dark">
      <Navbar.Brand href="#home">Note App</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link><NavLink className="nav-link" to='/' exact>Home</NavLink></Nav.Link>
          <Nav.Link><NavLink className="nav-link" to='/about'>About</NavLink></Nav.Link>
        </Nav>
        <Nav>
          {isAuth ?
            <Nav.Link>{email}</Nav.Link>
            : <Button variant='outline-light' onClick={() => { history.push('/registration') }}>Registration</Button>
          }

          {isAuth ?
            <Button variant='outline-light' onClick={() => { logInLogOut(false) }}>Sign out</Button>
            : <Button variant='outline-light' onClick={() => { history.push('/login') }}>Log in</Button>
          }

        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default NavbarMenu;
