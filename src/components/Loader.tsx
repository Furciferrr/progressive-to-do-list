import React from 'react'
import { Spinner } from 'react-bootstrap'


export const Loader = () => {
    return (
        <div className="text-center">
            <Spinner animation="grow" variant="primary" />
        </div>
    )
}