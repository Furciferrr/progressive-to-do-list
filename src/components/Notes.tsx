import React from 'react';
import { Button, ListGroup } from 'react-bootstrap';
import { NoteType } from '../context/firebase/firebaseReducer';
import { TransitionGroup, CSSTransition } from 'react-transition-group'

type NotePropsType = {
  notes: Array<NoteType>
  removeNote: (id: string, userId: string) => void
  userId: string
}
const Note: React.FC<NotePropsType> = (props) => {

  return (
    
      <ListGroup>
        <TransitionGroup className='list-group'>
        {props.notes.map(item => {
          return (
            <CSSTransition
              key={item.id}
              classNames={'noteAnimate'}
              timeout={800}
            >
              <ListGroup.Item className='note' variant="info" >
                <div>
                  <strong>{item.title}</strong>
                  <small>{item.date}</small>
                </div>
                <Button variant="danger" onClick={() => { props.removeNote(item.id, props.userId) }}>	&#215;</Button>
              </ListGroup.Item>
            </CSSTransition>
          )
        })}
        </TransitionGroup>
      </ListGroup>
    
  );
}

export default Note;
