import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AlertComponent from './components/Alert';
import { Login } from './components/login/Login';
import { Registration } from './components/login/Registration';
import Navbar from './components/Navbar';
import AlertState from './context/alert/AlertState';
import FirebaseState from './context/firebase/FirebaseState';
import About from './pages/About';
import Home from './pages/Home';

function App() {

  return (
    <FirebaseState>
      <AlertState>
        <BrowserRouter>
          <Navbar />
          <div className="container pt-4">
            <AlertComponent />
            <Switch>
              <Route path={'/'} exact component={Home} />
              <Route path={'/about'} component={About} />
              <Route path={'/login'} component={Login} />
              <Route path="/registration" render={() => <Registration  />} />
            </Switch>
          </div>
        </BrowserRouter>
      </AlertState>
    </FirebaseState>
  );
}

export default App;
